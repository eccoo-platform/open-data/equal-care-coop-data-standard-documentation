# Equal Care Coop Data Standard

This is a little sphinx project that can produce a nicely formatted set of docs.

All of the docs content should go in [docs](./docs).

## Installing

```sh
python3 -m venv venv
source ./venv/bin/activate
pip install sphinx sphinx-rtd-theme
```

## Building Locally

```sh
sphinx-build -b html docs public
```

## Running Locally

```sh
python -m http.server --directory=public
```

