Open Contracting Data Standard (OCDS) information
=================================================

Schema
------


A list of release packages can be provided using a standard `OCDS (Open Contracting Data Standard) <https://standard.open-contracting.org/latest/en/>`_ release package model.


Using data from other portals
-----------------------------

It is possible to use releases from other portals (eg "tender" releases) then for this publisher to publish their own releases later (eg "contract" or "implementation"  releases) on the same `Contracting Process Identifier (ocid) <https://standard.open-contracting.org/latest/en/schema/identifiers/#contracting-process-identifier-ocid>`_.

For releases from other portals, copy the release completely. Note associated metadata from the package, like extensions.

When republishing it, add a links block to the release to refer back to the where it came from:

.. code-block:: json

    "links": [
        {
          "rel": "canonical",
          "href": "https://api.publiccontractsscotland.gov.uk/v1/Notice?id=ocds-r6ebe6-0000517932"
        }
      ]

The links block will be added to OCDS in 1.2


Schema version
--------------

Our own OCDS releases should be created and published in the highest version possible (currently 1.1).

This is so we can make all releases in a contracting process the same version before merging, and `a tool already exists to convert 1.0 to 1.1 but not vice versa <https://github.com/open-contracting/ocdskit>`_.


Including metrics
-----------------

Sometimes we can attach metrics to a contract process. In this case, the release package should simply declare `the metrics extension <https://extensions.open-contracting.org/en/extensions/metrics/master/>`_ and data included using that. In this case, metrics could be in:

* "Planning" - "forecasts"
* "Tender" - "targets"
* "Award" - "agreedMetrics"
* "Contract" - "agreedMetrics"
* "Implementation" - "metrics"

:doc:`The definition of each metric <../metrics/definitions/index>` should tell you which place it could be in.

Merging releases for a contracting process into different types of records
--------------------------------------------------------------------------

Remember these releases may come from different places and use different versions of the standard and/or different extensions.

If releases are different versions, upgrade all releases to the latest version (currently 1.1) first using existing OCDS tools.

If releases use different extensions, make a list of all extensions and use that list when merging.

Then use standard OCDS `methods <https://standard.open-contracting.org/latest/en/schema/merging/#>`_  and
`tools <https://pypi.org/project/ocdskit/>`_ as normal.


Tool: OCDS Additions
--------------------

`OCDS Additions <https://pypi.org/project/ocdsadditions/>`_ is a tool that is used for managing OCDS releases.

We recommend using this tool to manage and publish this information online.
