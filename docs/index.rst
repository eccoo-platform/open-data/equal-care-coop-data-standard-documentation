Equal Care Coop Data Standard
=============================

The Data Standard providers ways for providers of social care to provide several bits of information about themselves.



.. toctree::
   :maxdepth: 3

   party/index.rst
   ocds/index.rst
   metrics/index.rst
   metrics/definitions/index.rst

