Information on the provider (Party information)
===============================================

Every provider should provide basic information about themselves.

Schema
------

This is a single JSON object.

This is based on the relevant fields from `an OCDS Party <https://standard.open-contracting.org/latest/en/schema/reference/#parties>`_.

Object should include:

* `name`, with `this being a value as defined in OCDS <https://standard.open-contracting.org/latest/en/schema/reference/#parties>`_
* `identifier` (Single value) & `additionalIdentifiers` (array), with `these being blocks as defined in OCDS <https://standard.open-contracting.org/latest/en/schema/reference/#identifier>`_
* `address`, with `this being a block as defined in OCDS <https://standard.open-contracting.org/latest/en/schema/reference/#address>`_
* `contactPoint`, with `this being a block as defined in OCDS <https://standard.open-contracting.org/latest/en/schema/reference/#contactpoint>`_


It should be packaged in a package object. Which can hold meta information about the object. This package object should include

* `socialCareProvider` - the block above
* For now, nothing else but other meta information can be included later when relevant eg version, uri

Unlike OCDS, these are not versioned in a release/record system. A publisher just publishes the latest values.

Tool: OCDS Additions
--------------------

`OCDS Additions <https://pypi.org/project/ocdsadditions/>`_ is a tool that is used for managing OCDS releases,
and as part of that can manage publisher information including a party block.

We recommend using this tool to manage and publish this information online.
