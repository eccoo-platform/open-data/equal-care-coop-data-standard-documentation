Metrics information
===================

Schema
------

There is a need to attach metrics outside a contract.

This uses `the Metric block from the OCDS Metric extension <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#metric>`_.

These are available in a package object, with the following:

* `metrics` - an array of metric objects as defined in the extension
* For now, nothing else but other meta information can be included later when relevant eg version, uri

Unlike OCDS, these are not versioned in a release/record system. A publisher just publishes the latest values.

:doc:`Each metric is defined <../metrics/definitions/index>`.


Tool: OCDS Metrics Analysis
---------------------------

`OCDS Metrics Analysis <https://pypi.org/project/ocdsmetricsanalysis/>`_ is a tool that is used for creating and querying metrics data in the required Metric/Observation JSON objects format.

We recommend using this tool to create the data to publish.

This tool itself does not manage or publish data that has been created however. As these files are simple JSON files, there are many options to do this.

We also recommend that people use this tool in `Jupyter <https://jupyter.org/>`_ or `Google Colab <https://colab.research.google.com/>`_ notebooks to analyse published data.
