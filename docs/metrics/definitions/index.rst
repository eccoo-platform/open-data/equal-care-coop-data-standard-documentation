Metrics definitions
===================



Metric objects have an id, which states "`In some cases this may be drawn from a codelist of metrics required for this type of contracting process <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#metric>`_". This is the case here.

Each definition should include:

* `id` - see field in `metric schema <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#metric>`_
* `title` - see field in `metric schema <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#metric>`_
* `description`  - see field in `metric schema <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#metric>`_
* Unit `name`, `scheme`, `id` & `uri` -  see field in `observation schema <https://extensions.open-contracting.org/en/extensions/metrics/master/schema/#observation>`_
* Dimensions - a list of dimensions that are possible. For each one, define
    * The key
    * Possible values, be that by codelist, type of value (id, string) or any other rule that may apply eg a list of geographical boundaries like english councils
* Context (multiple answers possible here) - Whether this is expected outside or inside a contract. If it’s expected inside a contract, which of the 5 possible places it may be in:
    * "Planning" - "forecasts"
    * "Tender" - "targets"
    * "Award" - "agreedMetrics"
    * "Contract" - "agreedMetrics"
    * "Implementation" - "metrics"

Definitions of metrics can be found in the following pages:

.. toctree::
   :maxdepth: 3

   equal-care-coop-survey.rst


