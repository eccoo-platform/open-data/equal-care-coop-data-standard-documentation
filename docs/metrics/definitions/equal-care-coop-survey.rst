Metrics definitions from Equal Care Co-op Survey
================================================

Metrics are aggregates
----------------------

All answers are in some ways aggregates across all surveys eg “how many survey responses answered __ to question ___”

Units
-----

Units are therefore:


.. list-table:: Units

   * - unit.name
     - Number
   * - unit.scheme
     - QUDT
   * - unit.id
     - 1250
   * - unit.uri
     - *none*

Answer Dimension
----------------

For every question, there is therefore a dimension called “answer” that records the answer given.

Answer dimension for boolean questions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table::
   :header-rows: 1

   * - Dimension Key
     - Dimension Value
   * - answer
     - yes
   * - answer
     - no


Answer dimension for slider (Livert)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This one is complicated as different metrics have different answers and some have different number of answers (4 or 5).

For general 1 to 5 or 1 to 4 answers:

.. list-table::
   :header-rows: 1

   * - Dimension Key
     - Dimension Value
   * - answer
     - 1
   * - answer
     - 2
   * - answer
     - 3
   * - answer
     - 4
   * - answer
     - 5



For metrics with defined answer strings, the number is always given with a dash then the string. For instance:


.. list-table::
   :header-rows: 1

   * - Dimension Key
     - Dimension Value
   * - answer
     - 1 - Very unsafe
   * - answer
     - 2 - Quite unsafe
   * - answer
     - 3 - Okay
   * - answer
     - 4 - Quite safe
   * - answer
     - 5 - Very safe



Other Dimensions
----------------

*TODO*

Time Periods
------------

*WHAT WOULD BE A GOOD BLOCK OF TIME TO GROUP THESE BY?*

Context
-------

These metrics are used outside a contract.

"About your experience of your Team" section
--------------------------------------------

Answers to these are boolean values.


.. list-table::
   :header-rows: 1

   * - Id
     - Title
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_CHOOSE_WHO_JOINS
     - About your experience of your Team >> I choose who joins my Team
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_SAID_NO_TO_MATCH
     - About your experience of your Team >> I have said no to a possible match after an introduction or several sessions
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_KNOW
     - About your experience of your Team >> I know who is in my Team
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_GET_IN_TOUCH
     - About your experience of your Team >> I can easily get in touch with my Team members when I want to
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_MY_OWN_ROLE
     - About your experience of your Team >> I have a clear sense of my own role within my Team
   * - ECC_SURVEY_EXPERIENCE_YOUR_TEAM_SAFE_RAISE_ISSUES
     - About your experience of your Team >> I feel safe to raise issues or concerns and ask for someone to leave my Team if I want them to


"Short Warwick-Edinburgh Mental Wellbeing Scale (SWEMWS)"
---------------------------------------------------------


Answers to these are boolean values


.. list-table::
   :header-rows: 1

   * - Id
     - Title
   * - *TODO*
     - Has your family member or friend's wellbeing changed as a result of their involvement with Equal Care?
   * - *TODO*
     - Has your wellbeing changed as a result of their involvement with Equal Care?    


Answers to these are on a scale of 1 to 5. Where 

1 = None of the time, 
2 = Rarely, 
3 = Some of the time, 
4 = Often, 
5 = All of the time, 


.. list-table::
   :header-rows: 1

   * - Id
     - Title
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Feeling optimistic about the future
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Feeling useful
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Feeling relaxed
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Dealing with problems well
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Thinking clearly
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Feeling close to other people
   * - *TODO*
     - Please tell us about their wellbeing before their involvement with Equal Care >> Able to make up their mind about things  
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Feeling optimistic about the future
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Feeling useful
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Feeling relaxed
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Dealing with problems well
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Thinking clearly
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Feeling close to other people
   * - *TODO*
     - Please tell us about their wellbeing as it is now >> Able to make up their mind about things 
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Feeling optimistic about the future
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Feeling useful
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Feeling relaxed
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Dealing with problems well
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Thinking clearly
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Feeling close to other people
   * - *TODO*
     - Please tell us about your wellbeing before your involvement with Equal Care >> Able to make up my mind about things  
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Feeling optimistic about the future
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Feeling useful
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Feeling relaxed
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Dealing with problems well
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Thinking clearly
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Feeling close to other people
   * - *TODO*
     - Please tell us about your wellbeing as it is now >> Able to make up my mind about things
      


"About your experience of being in Teams" section
-------------------------------------------------


Answers to these are boolean values.


.. list-table::
   :header-rows: 1

   * - Id
     - Title
   * - *TODO*
     - About your experience of being in Teams >> I choose which Teams to join
   * - *TODO*
     - About your experience of being in Teams >> I feel free to say no to a possible match after an introduction or several sessions
   * - *TODO*
     - About your experience of being in Teams >> I know who the other Team members are
   * - *TODO*
     - About your experience of being in Teams >> I can easily get in touch with Team members when I want to
   * - *TODO*
     - About your experience of being in Teams >> I have a clear sense of my own role within my Teams
   * - *TODO*
     - About your experience of being in Teams >> I feel safe to leave Teams I've joined if I want to




Unknown section, row 85 onwards
-------------------------------

These are all Slider (Likert) type answers

.. list-table::
   :header-rows: 1

   * - Id
     - Title
     - Answers
   * - *TODO*
     - I feel supported to reach the life or care and support experience I want.
     - 1 - 5
   * - *TODO*
     - I feel that my relative or friend is being supported to reach the life or care and support experience they want.
     - 1 - 5
   * - *TODO*
     - I feel supported to reach the livelihood and work / life balance I want.
     - 1 - 5
   * - *TODO*
     - I feel well supported as a volunteer giving care and support.
     - 1 - 5
   * - *TODO*
     - How much work do you put in to manage the care and support with Equal Care?
     -
   * - *TODO*
     - I feel that I am in control of the care and support that I receive
     -
   * - *TODO*
     - I am happy with the level of control I have over the care and support I receive
     - 1 - 5
   * - *TODO*
     - I feel that I am in control of my role and the care and support I provide
     - I am not at all in control; I have control over some things; I have control over most things; I am fully in control
   * - *TODO*
     - I am happy with the level of control I have over my role and the care and support I provide
     - 1 - 5
   * - *TODO*
     - How safe do you feel with Equal Care?
     - Very unsafe; Quite unsafe; Okay; Quite safe; Very safe
   * - *TODO*
     - How trusted do you feel by Equal Care facilitators?
     - Not at all trusted; Slightly trusted; Quite trusted; Extremely trusted
   * - *TODO*
     - How trusted do you feel on average by the people you support and their family?
     - Not at all trusted; Slightly trusted; Quite trusted; Extremely trusted
   * - *TODO*
     - Have you ever felt pressured to accept a match or attend a care and support booking?
     - All the time; Often; About half the time; Rarely; Never
   * - *TODO*
     - How would you rate the overall quality of the relationships with your Team members?
     - 1 - 5
   * - *TODO*
     - Have you felt more socially connected since become involved with Equal Care?
     - Much less socially connected; A bit less socially connecteed; No change; A bit more socially connected; Much more socially connected

